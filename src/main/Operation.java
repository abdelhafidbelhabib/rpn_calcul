package main;

public enum Operation {

	Plus('+') {
		public double eval(double x, double y) {
			return x + y;
		}
	},
	Moins('-') {
		public double eval(double x, double y) {
			return x - y;
		}
	},
	Mult('*') {
		public double eval(double x, double y) {
			return x * y;
		}
	},
	Div('/') {
		public double eval(double x, double y) throws ArithmeticException {
			if (y == 0)
				throw new ArithmeticException("!!!! ** vous avez essayer de diviser par 0 ** !!!!");
			else
				return x / y;
		}
	};
	private char symbole;

	// Constructeur qui prend
	Operation(char symbole) {

		String allOp = "+-*/";

		if (allOp.indexOf(symbole) == -1)
			throw new IllegalArgumentException();
		else
			this.symbole = symbole;
	}

	public char getSymbol() {
		return symbole;
	}

	public abstract double eval(double x, double y) throws ArithmeticException;
}
