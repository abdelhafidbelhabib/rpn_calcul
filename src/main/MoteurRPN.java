package main;

import java.util.Stack;

public class MoteurRPN {
	private Stack<Double> PileOperandes;

	public MoteurRPN() {
		PileOperandes = new Stack<Double>();
	}

	// Permet d'ajouter une opérande
	public void AddOperande(double val) {
		PileOperandes.add(val);
	}

	// renvoi une Operandes
	public Stack<Double> getOperandes() {
		return PileOperandes;
	}

	// pour appliquer une operation donné en parametre
	public void AppliquerOperation(Operation Operation) throws ArithmeticException {
		if (PileOperandes.size() >= 2) {

			double valeur_2 = (PileOperandes).pop();

			double valeur_1 = (PileOperandes).pop();

			PileOperandes.add(Operation.eval(valeur_1, valeur_2));
		}

		else
			throw new ArithmeticException("!!!! ***le nombres d'opérandes ne dépasse pas 2*** !!!!");
	}

	public void afficherOperandes() {
		System.out.print("[||");
		for (int i = 0; i < PileOperandes.size(); i++) {
			if (i == PileOperandes.size() - 1) {
				System.out.print(PileOperandes.get(i));
				break;
			}
			System.out.print(PileOperandes.get(i) + "|==>|");
		}
		System.out.print("||]");
	}

}
