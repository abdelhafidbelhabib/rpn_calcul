package main;

import java.util.Scanner;

class SaisieRPN {
	/*
	 * déclarion des deux variables désigant l'intervalle des valeurs possible à
	 * prendre
	 */

	public final static int MAX_VALUE = 9000000;
	public final static int MIN_VALUE = -9000000;

	// le clavier
	private Scanner clavier = new Scanner(System.in);
	public MoteurRPN moteur = new MoteurRPN();

	/*
	 * méthode qui permet à l'utilisateur d'inserer des chiffres et des opérandes
	 * appel à la fonction AppelMoteurRPN
	 */
	public void Ecrire() {

		AppelMoteurRPN("0");
		System.out.println("-----<   Calculette RPN   >-----");
		while (true) {
			String valeur = clavier.nextLine();
			try {

				AppelMoteurRPN(valeur);
			} catch (ArithmeticException | NumberFormatException incorrecte) {

				System.out.println("Saisie incorrecte : " + incorrecte.getMessage());
				moteur.afficherOperandes();
			}
		}
	}

	/*
	 * méthode qui fait appel à la méthode AppliquerOperation(Operation Operation)
	 * et afficherOperandes() si la saisie de l'utilisateur est correcte à default
	 * renvoyer un message d'erreur
	 */
	private void AppelMoteurRPN(String valeur) throws NumberFormatException, ArithmeticException {

		if (valeur.equals("+")) {

			moteur.AppliquerOperation(Operation.Plus);
			moteur.afficherOperandes();

		} else {
			if (valeur.equals("-")) {
				moteur.AppliquerOperation(Operation.Moins);
				moteur.afficherOperandes();

			} else {
				if (valeur.contentEquals("*")) {
					moteur.AppliquerOperation(Operation.Mult);
					moteur.afficherOperandes();

				} else {

					if (valeur.equals("/")) {
						moteur.AppliquerOperation(Operation.Div);
						moteur.afficherOperandes();

					} else {
						if (valeur.equals("exit")) {
							System.exit(1);

						} else {
							double oper = Double.parseDouble(valeur);
							if (oper > MIN_VALUE && oper < MAX_VALUE) {
								moteur.AddOperande(oper);
								moteur.afficherOperandes();

							} else {

								throw new ArithmeticException("\n Le nombre que vous avez saisie n'est pas autorisé");

							}
						}

					}

				}

			}

		}
	}
}
