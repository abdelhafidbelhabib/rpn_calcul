package test;

import main.MoteurRPN;

import main.Operation;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
public class AllTest {
	private MoteurRPN Moteur ;
	//déclarer l'objet Moteur avant le lancement des tests
	@Before
	public void initialize() {
	    Moteur = new MoteurRPN();
		
	}
	
	
	// au cas ou on dévise par 0 si l'exception est lever 
	@Test (expected = ArithmeticException.class)
	public void TestDivisionSurZero(){
		
		double val1=3,val2=0;
		Moteur.AddOperande(val1);
		Moteur.AddOperande(val2);
		Moteur.AppliquerOperation(Operation.Div);
		
		
	}
	//test sur l'operateur "+"

	@Test
	public void testPlus() {
		
		
		Moteur.AddOperande(3);
		Moteur.AddOperande(27);
		Moteur.AppliquerOperation(Operation.Plus);
		
		assertTrue("l'operation Plus a echouer ",Moteur.getOperandes().get(0)==30.0);
		
	}
	

	//test sur l'operateur "-"

	@Test
	public void testMoins() {
	
		Moteur.AddOperande(5);
		Moteur.AddOperande(6);
		Moteur.AppliquerOperation(Operation.Moins);
		
		assertTrue("l'operation Moins a echouer ",Moteur.getOperandes().get(0)==-1.0);
		
	}

	//test sur l'operateur "/"

	@Test
	public void testDiv() {
		
		
		Moteur.AddOperande(1);
		Moteur.AddOperande(2);
		Moteur.AppliquerOperation(Operation.Div);
		
		assertTrue("l'operation Div a echouer ",Moteur.getOperandes().get(0)==0.5);
		
	}
	//test sur l'operateur "*"

	@Test
	public void testMultipliction() {
		
		
		Moteur.AddOperande(3);
		Moteur.AddOperande(2);
		Moteur.AppliquerOperation(Operation.Mult);
		
		assertTrue("l'operation Multiplication a echouer ",Moteur.getOperandes().get(0)==6.0);
		
	}

	//On test le constructeur Operation && getSymbol
			@Test 
			public void TestOperation(){
				
				Operation O = Operation.Mult;
				assertEquals(O.getSymbol(),'*');
				
			}
			

	
	
	
	
	
	
	
	
}
