package test;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import main.MoteurRPN;
import main.Operation;


public class TestMoteur {

	private MoteurRPN moteur;
	
	@Test
	public void testOperande() {
		moteur = new MoteurRPN();
		assertTrue(moteur.getOperandes().isEmpty());
	}
	
	
	@Before
	public void initialiser() {
		
		moteur = new MoteurRPN();
	}
	
	
	//  tester si le MoteurRPN ajoute  les operandes
		@Test
		public void testEnregistrerOperande() {
			double valeur = 10;
			double valeur1 = 5;
			moteur.AddOperande(valeur);
			moteur.AddOperande(valeur1);

			assertEquals(moteur.getOperandes().size(), 2);
		}

		//  test si AppliquerOperation lance une Exception
		@Test(expected = ArithmeticException.class)
		
		public void testAppliquerOperationException() {
			
			moteur.AppliquerOperation(Operation.Plus);
		}
	
		// tester si le Moteur verifie qu'on a pas assez suffisament d'operandes
		@Test(expected = ArithmeticException.class)
		
		public void testAddOperandeException() {

			double valeur = 7;
			moteur.AddOperande(valeur);
			moteur.AppliquerOperation(Operation.Plus);

		}
	
}
